# cert-manifest
When you have to get SSL/TLS certificate from cert authorization,
You should make some application that has specific path and file.
This manifests will make some application that has specific path and file.

# Component
- cert-manifest.yaml : make a file and path with configmap
- cert-manifest-txt.yaml : make a file and path without configmap

# Keep to know
After getting your certification from cert authorization, 
Delete the application that made by this manifest.
